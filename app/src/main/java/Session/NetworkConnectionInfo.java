package Session;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by root on 9/1/17.
 */

public class NetworkConnectionInfo {

    public static boolean CheckWifiConnection(Context mContect) {
    ConnectivityManager connManager = (ConnectivityManager)mContect.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

       if(mWifi.isConnected())
      {
        return true;
      }

      return false;
    }

    public static boolean isInternetAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
